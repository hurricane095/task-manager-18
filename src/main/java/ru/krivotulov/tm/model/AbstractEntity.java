package ru.krivotulov.tm.model;

import java.util.UUID;

/**
 * AbstractEntity
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractEntity {

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
