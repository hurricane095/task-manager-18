package ru.krivotulov.tm.command.system;

import ru.krivotulov.tm.api.service.ICommandService;
import ru.krivotulov.tm.command.AbstractCommand;

/**
 * AbstractSystemCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
