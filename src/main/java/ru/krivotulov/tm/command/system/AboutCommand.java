package ru.krivotulov.tm.command.system;

/**
 * AboutCommand
 *
 * @author Aleksey_Krivotulov
 */
public class AboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Display developer info.";

    public static final String ARGUMENT = "-a";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Aleksey Krivotulov");
        System.out.println("akrivotulov@tsconsulting.com");
    }

}
