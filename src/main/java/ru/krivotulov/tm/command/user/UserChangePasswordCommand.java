package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserChangePasswordCommand
 *
 * @author Aleksey_Krivotulov
 */
public class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "user-change-password";

    public static final String DESCRIPTION = "Change password for user by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD: ");
        final String password = TerminalUtil.readLine();
        getUserService().setPassword(userId, password);
    }

}
