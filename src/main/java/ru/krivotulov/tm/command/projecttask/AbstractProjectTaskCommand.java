package ru.krivotulov.tm.command.projecttask;

import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.command.AbstractCommand;

/**
 * AbstractProjectTaskCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return null;
    }

    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }
}
