package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskUpdateByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-id";

    public static final String DESCRIPTION = "Update task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        getTaskService().updateById(id, name, description);
    }

}
