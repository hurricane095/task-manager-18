package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;

import java.util.List;

/**
 * IUserRepository
 *
 * @author Aleksey_Krivotulov
 */
public interface IUserRepository {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User add(User user);

    User findById(String id);

    User removeUser(User user);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);
}
