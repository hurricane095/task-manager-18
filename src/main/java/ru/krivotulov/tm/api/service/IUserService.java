package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;

import java.util.List;

/**
 * IUserService
 *
 * @author Aleksey_Krivotulov
 */
public interface IUserService {

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    Boolean isLoginExist(String login);

    User findByEmail(String email);

    Boolean isEmailExist(String email);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(String userId,
                    String firstName,
                    String lastName,
                    String middleName);

}
