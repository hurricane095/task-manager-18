package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * UserRepository
 *
 * @author Aleksey_Krivotulov
 */
public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User findByLogin(String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeById(String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return findByEmail(email) != null;
    }

}
